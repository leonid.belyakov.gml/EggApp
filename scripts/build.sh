#!/bin/bash

BRANCH="$CI_COMMIT_REF_NAME"
BRANCH=${BRANCH#"origin/"}

case "$BRANCH" in
"master") # Собрать сборку с типом release
    echo "${KEY_STORE_PROP}" | base64 -d > key.properties
    echo "${KEYSTORE_FILE}" | base64 -d > app/keystore.jks
    ./gradlew bundleRelease --stacktrace || exit 1
    ./scripts/newtag.sh release
    ;;
*) # Собрать сборку с типом debug
    ./gradlew assembleDebug --stacktrace || exit 1
    ./scripts/newtag.sh debug
    ;;
esac