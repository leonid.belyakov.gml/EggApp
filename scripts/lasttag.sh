#!/bin/bash

TAG=$(git describe --tags --abbrev=0 "$@")
COMMIT=$(git rev-list -n 1 "$TAG")
ALL_TAGS=$(git show-ref --tags -d | grep "$COMMIT" | sed -e 's,.* refs/tags/,,' -e 's/\^{}//')
# Для случая, когда на одном коммите сразу несколько тегов
LAST_TAG=$(echo "$ALL_TAGS" | sort -gr | sed -n '1p')
echo "$LAST_TAG"