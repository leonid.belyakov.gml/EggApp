#!/bin/bash

BRANCH="$CI_COMMIT_REF_NAME"
if [ -z "$BRANCH" ]; then
  echo "BRANCH is not defined! The release notes will be empty."
  exit
fi
BRANCH=${BRANCH#"origin/"}

case $BRANCH in
"develop" | "master")
  TAG=$(git describe --tags --abbrev=0)
  SUFFIX="$TAG..HEAD"
  ;;
*)
  SUFFIX="origin/develop..origin/$BRANCH"
  ;;
esac

LOG=$(git log --pretty="%s" --no-merges "$SUFFIX")
echo "$LOG"