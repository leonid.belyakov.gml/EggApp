#!/bin/bash

RELEASE_NOTES=$(cat ./build_data/release-notes.txt || exit 1)

echo "Release notes:"
echo "$RELEASE_NOTES"

BRANCH="$CI_COMMIT_REF_NAME"
BRANCH=${BRANCH#"origin/"}

case "$BRANCH" in
"master") # Отправить release в Google Play, AppCenter
  ;;
*) # Отправить debug в AppCenter
  ;;
esac