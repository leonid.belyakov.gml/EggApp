#!/bin/bash

BRANCH="$CI_COMMIT_REF_NAME"
BRANCH=${BRANCH#"origin/"}

case "$BRANCH" in
"master") # Прогнать lint для release
    echo "${KEY_STORE_PROP}" | base64 -d > key.properties
    echo "${KEYSTORE_FILE}" | base64 -d > app/keystore.jks
    ./gradlew lintRelease --stacktrace || exit 1
    ;;
*) # Прогнать lint для debug
    ./gradlew lintDebug --stacktrace || exit 1
    ;;
esac