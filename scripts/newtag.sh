#!/bin/bash
BUILD_TYPE="$1"

if [ -z "$VERSION_NAME" ]; then
    PREV_TAG=$(./scripts/lasttag.sh --match v*)
    VERSION_NAME=$(echo "$PREV_TAG" | grep -oE 'v[0-9]+\.[0-9]+\.[0-9]+')
else
    VERSION_NAME="v$VERSION_NAME"
fi

INCREMENT=$(git describe --tags --abbrev=0 --match "$VERSION_NAME".* 2>/dev/null | grep -oE '[0-9]+[^0-9\.]+' | grep -oE '[0-9]+' || echo "-1")

((INCREMENT = INCREMENT + 1))
NEW_TAG="$VERSION_NAME.$INCREMENT-$BUILD_TYPE"

# проверяем, нет ли уже тега с таким именем
while git rev-parse "$NEW_TAG" >/dev/null 2>&1; do
    ((INCREMENT = INCREMENT + 1))
    NEW_TAG="$VERSION_NAME.$INCREMENT-$BUILD_TYPE"
done

if [ -z "$CI_COMMIT_SHA" ]; then
    echo "Commit for new tag $NEW_TAG is not specific, skipping"
    exit 0
fi

echo "Created new tag $NEW_TAG"
git tag "$NEW_TAG" "$CI_COMMIT_SHA"

git remote set-url origin "https://gitlab-ci-token:$GITLAB_ACCESS_TOKEN@gitlab.com/leonid.belyakov.gml/EggApp.git"
git push origin "$NEW_TAG" --tags