#!/bin/bash
BRANCH="$CI_COMMIT_REF_NAME"
BRANCH=${BRANCH#"origin/"}

case "$BRANCH" in
"master") # Прогнать тесты для release
    echo "${KEY_STORE_PROP}" | base64 -d > key.properties
    echo "${KEYSTORE_FILE}" | base64 -d > app/keystore.jks
    ./gradlew testReleaseUnitTest --stacktrace || exit 1
    ;;
*) # Прогнать тесты для debug
    ./gradlew testDebugUnitTest --stacktrace || exit 1
    ;;
esac