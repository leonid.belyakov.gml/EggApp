object Versions {
    const val kotlin = "1.7.10"
    const val viewBindingDelegate = "1.5.0-beta01"
    const val gradle = "7.2.1"
    const val navigation = "2.5.0"
    const val junit = "4.13.2"
    const val androidJunit = "1.1.3"
    const val espresso = "3.4.0"
    const val jetpackCore = "1.8.0"
    const val jetpackFragment = "1.5.0"
    const val constraint = "2.1.4"
    const val material = "1.7.0-alpha03"
    const val konfetti = "1.2.2"
    const val dagger = "2.42"

    object Firebase {
        const val core = "4.3.13"
        const val bom = "30.2.0"
        const val crashlytics = "2.9.1"
        const val distribution = "3.0.2"
    }
}