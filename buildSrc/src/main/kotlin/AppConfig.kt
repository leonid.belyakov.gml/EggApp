object AppConfig {
    const val compileSdk = 32
    const val targetSdk = 32
    const val minSdk = 23
}