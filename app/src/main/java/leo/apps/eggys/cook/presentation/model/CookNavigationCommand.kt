package leo.apps.eggys.cook.presentation.model

sealed class CookNavigationCommand {
    object PopUp : CookNavigationCommand()
    object ShowExitDialog : CookNavigationCommand()
}