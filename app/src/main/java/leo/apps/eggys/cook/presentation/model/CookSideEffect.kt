package leo.apps.eggys.cook.presentation.model

sealed class CookSideEffect {
    object Finish : CookSideEffect()
    object Cancel : CookSideEffect()
}