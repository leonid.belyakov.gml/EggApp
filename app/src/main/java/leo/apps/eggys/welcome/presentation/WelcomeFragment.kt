package leo.apps.eggys.welcome.presentation

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import leo.apps.eggys.R
import leo.apps.eggys.base.analytics.Analytics
import leo.apps.eggys.base.presentation.BaseFragment
import leo.apps.eggys.base.utils.getInjector
import leo.apps.eggys.databinding.FWelcomeBinding

class WelcomeFragment : BaseFragment(R.layout.f_welcome) {
    override val screenName = Analytics.Welcome.SCREEN_NAME
    override val viewModel: WelcomeViewModel by viewModels { viewModelFactory }
    private val binding by viewBinding(FWelcomeBinding::bind)

    override fun onAttach(context: Context) {
        super.onAttach(context)
        getInjector().inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.button.setOnClickListener {
            viewModel.onButtonClick()
            findNavController().navigate(R.id.actionToSetupScreen)
        }
    }
}