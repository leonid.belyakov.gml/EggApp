package leo.apps.eggys.base.utils

fun Float.toRadians(): Float = (this / 180f) * Math.PI.toFloat()