package leo.apps.eggys.base.di

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import leo.apps.eggys.base.di.modules.BaseModule
import leo.apps.eggys.base.di.modules.SetupModule
import leo.apps.eggys.base.di.modules.ViewModelModule
import leo.apps.eggys.cook.presentation.CookFragment
import leo.apps.eggys.setup.presentation.SetupFragment
import leo.apps.eggys.welcome.presentation.WelcomeFragment
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        BaseModule::class,
        ViewModelModule::class,
        SetupModule::class
    ]
)
interface AppComponent {
    fun inject(fragment: SetupFragment)
    fun inject(fragment: CookFragment)
    fun inject(fragment: WelcomeFragment)

    @Component.Factory
    interface Factory {
        fun create(
            @BindsInstance context: Context
        ): AppComponent
    }
}