package leo.apps.eggys.base.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import leo.apps.eggys.base.di.ViewModelFactory
import leo.apps.eggys.base.di.ViewModelKey
import leo.apps.eggys.cook.presentation.CookViewModel
import leo.apps.eggys.setup.presentation.SetupViewModel
import leo.apps.eggys.welcome.presentation.WelcomeViewModel

@Module
interface ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(CookViewModel::class)
    fun bindsCookViewModel(impl: CookViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SetupViewModel::class)
    fun bindsSetupViewModel(impl: SetupViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WelcomeViewModel::class)
    fun bindsWelcomeViewModel(impl: WelcomeViewModel): ViewModel

    @Binds
    fun bindsViewModelFactory(impl: ViewModelFactory): ViewModelProvider.Factory
}