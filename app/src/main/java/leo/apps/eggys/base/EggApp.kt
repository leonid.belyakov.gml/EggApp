package leo.apps.eggys.base

import android.app.Application
import leo.apps.eggys.base.di.AppComponent
import leo.apps.eggys.base.di.DaggerAppComponent

class EggApp : Application() {

    val appComponent: AppComponent by lazy {
        DaggerAppComponent.factory().create(applicationContext)
    }
}