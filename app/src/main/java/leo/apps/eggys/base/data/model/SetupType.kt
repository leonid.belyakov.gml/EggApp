package leo.apps.eggys.base.data.model

enum class SetupType {
    SOFT,
    MEDIUM,
    HARD
}