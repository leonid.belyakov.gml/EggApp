package leo.apps.eggys.base.presentation

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import androidx.navigation.findNavController
import leo.apps.eggys.R
import leo.apps.eggys.timer.TimerService

class MainActivity : AppCompatActivity(R.layout.a_main) {

    private val connection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {}

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            val timerBinder = service as? TimerService.TimerBinder
            if (timerBinder?.isRunning == true) {
                findNavController(R.id.mainContainer).let {
                    it.setGraph(R.navigation.content_graph)
                    it.navigate(R.id.cookFragment)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)
        startTimerService()
    }

    private fun startTimerService() {
        val intent = Intent(this, TimerService::class.java)
        startService(intent)
        bindService(intent, connection, BIND_AUTO_CREATE)
    }
}