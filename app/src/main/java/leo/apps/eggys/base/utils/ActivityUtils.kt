package leo.apps.eggys.base.utils

import android.app.Activity
import leo.apps.eggys.base.EggApp

fun Activity.getInjector() = (application as EggApp).appComponent