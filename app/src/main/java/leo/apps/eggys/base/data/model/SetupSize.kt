package leo.apps.eggys.base.data.model

enum class SetupSize {
    S,
    M,
    L
}
