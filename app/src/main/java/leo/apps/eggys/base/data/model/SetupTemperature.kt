package leo.apps.eggys.base.data.model

enum class SetupTemperature {
    ROOM,
    FRIDGE
}