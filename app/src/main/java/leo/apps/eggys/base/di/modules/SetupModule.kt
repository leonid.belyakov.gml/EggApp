package leo.apps.eggys.base.di.modules

import dagger.Binds
import dagger.Module
import leo.apps.eggys.base.data.SetupEggRepository
import leo.apps.eggys.base.data.SetupEggRepositoryImpl
import javax.inject.Singleton

@Module
interface SetupModule {
    @Binds
    @Singleton
    fun bindsSetupRepository(impl: SetupEggRepositoryImpl): SetupEggRepository
}