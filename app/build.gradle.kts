import java.io.FileInputStream
import java.util.Properties

plugins {
    id("com.android.application")
    id("androidx.navigation.safeargs")
    id("kotlin-parcelize")
    kotlin("kapt")
    kotlin("android")
    id("com.google.gms.google-services")
    id("com.google.firebase.crashlytics")
    id("com.google.firebase.appdistribution")
}

android {

    signingConfigs {
        val keyFile = File(rootProject.rootDir, "key.properties")
        if (keyFile.exists()) {
            val keystoreProperties = Properties().apply { load(FileInputStream(keyFile)) }
            create("release") {
                keyAlias = keystoreProperties.getProperty("keyAlias")
                keyPassword = keystoreProperties.getProperty("keyPassword")
                storeFile = file(keystoreProperties.getProperty("storeFile"))
                storePassword = keystoreProperties.getProperty("storePassword")
            }
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }
    kotlinOptions {
        jvmTarget = "11"
    }

    defaultConfig {
        applicationId = "leo.apps.eggys"
        compileSdk = AppConfig.compileSdk
        minSdk = AppConfig.minSdk
        targetSdk = AppConfig.targetSdk
        versionCode = 10
        versionName = "1.10"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildFeatures {
        viewBinding = true
    }

    buildTypes {
        getByName("release") {
            isShrinkResources = true
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            signingConfigs.findByName("release")?.let { config ->
                signingConfig = config
            }
            firebaseAppDistribution {
                appId = System.getenv("FIREBASE_PROJECT_ID")
                artifactType = "AAB"
                releaseNotesFile = "${rootProject.rootDir.path}/build_data/release-notes.txt"
                artifactPath = "${buildDir}/outputs/bundle/release/app-release.aab"
                groups = "QA"
            }
        }

        getByName("debug") {
            applicationIdSuffix = ".debug"
            versionNameSuffix = "-debug"

            firebaseAppDistribution {
                appId = System.getenv("FIREBASE_PROJECT_ID")
                artifactType = "APK"
                releaseNotesFile = "${rootProject.rootDir.path}/build_data/release-notes.txt"
                artifactPath = "${buildDir}/outputs/apk/debug/app-debug.apk"
                groups = "QA"
            }
        }
    }
}

dependencies {
    implementation(Dependencies.kotlin)
    implementation(Dependencies.viewBindingDelegate)
    implementation(Dependencies.Tests.junit)
    implementation(Dependencies.Tests.androidJunit)
    implementation(Dependencies.Tests.espresso)
    implementation(Dependencies.Jetpack.core)
    implementation(Dependencies.Jetpack.fragment)
    implementation(Dependencies.Ui.constraint)
    implementation(Dependencies.Ui.material)
    implementation(Dependencies.Ui.konfetti)
    implementation(Dependencies.Navigation.navigation)
    implementation(Dependencies.Dagger.dagger)
    kapt(Dependencies.Dagger.daggerCompiler)
    implementation(platform(Dependencies.Firebase.base))
    implementation(Dependencies.Firebase.analytics)
    implementation(Dependencies.Firebase.crashlytics)
}